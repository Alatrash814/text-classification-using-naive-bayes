package text.calssification;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Vector;
import javafx.util.Pair;
import jdk.nashorn.internal.objects.Global;

public class TextCalssification {
    
    
    //HashMaps
    private static HashMap <String,Integer> stopWords = new HashMap<String,Integer>();
    
    private static HashMap <String , HashMap <String,Integer>> allTrainigWords = 
            new HashMap <String , HashMap<String, Integer>>(); 
    
    private static HashMap <String,Double> prior = new HashMap <String,Double> ();
    private static HashMap <String,Integer> allWordsFormAllTraining = new HashMap<String,Integer>();
    private static HashMap <String,Long> wordConterForEveryClass = new HashMap <String,Long> ();
    
    private static HashMap <String , HashMap<String,Integer>> allTestWords = 
            new HashMap <String , HashMap<String, Integer>>(); 
    
    private static HashMap <String,HashMap<String,Double>> allTestFilesProb =
            new HashMap <String,HashMap<String,Double>>();
    private static HashMap <String,String> testPredection = new HashMap<String,String>();
    
    private static HashMap <String,HashMap<String,Integer>> conf = new HashMap<String,HashMap<String,Integer>>();
    
    //vectors
    private static Vector <String> lineAfterStemming = new Vector<>();
   
    
    //File Reders & Writers
    private static BufferedReader BR = null;
    private static BufferedWriter BW = null;
    
    //String
    private static String tempLine = null;
    private static String FileName = null;
    private static String [] wordsInLine = null;
    private static String wordAfterStemming = null;
    private static String classNameForTest = null;
    
    //Integers
    private static Integer priorCounter = 0;
    private static Integer numberOfWords = 0;
    
    
    //Double
    private static Double prevProb = 0D;
    private static Double addToProb = 0D;
    private static Double prob = 0D;
    private static Double totalF_Score = 0D;
    private static Double precision = 0D;
    private static Double recall = 0D;
    private static Double F_Score = 0D;
    private static Double truePositive = 0D;
    private static Double falsePositive = 0D;
    private static Double trueNegative = 0D;
    private static Double falseNegative = 0D;
    
    
    public static void main(String[] args) throws IOException {

        
        System.out.println("Reading stop words"); 
        readStopWords();
        System.out.println("Finish reading stop words \n");
        
        
        System.out.println("Reading training data set");
        filesInDirTraining(new File("Reuters21578-Apte-90Cat/training"));
        System.out.println("Finish Reading training data set \n");
        
        
        System.out.println("Calcualte prior for every class");
        for (String temp : prior.keySet())
            prior.put(temp, prior.get(temp) / priorCounter);
        System.out.println("Finish calcualting parior \n");                                         
        
        
        System.out.println("Writing training words counter to text files");
        //writeTrainingWordsCountersToFiles();
        System.out.println("Finish writing training words counter \n");
        
        
        System.out.println("Reading test data set");
        filesInDirTest(new File("Reuters21578-Apte-90Cat/training"));
        System.out.println("Finish Reading test data set \n");
        
        
        System.out.println("Writing test words counter to text files");
        //writeTestWordsCountersToFiles();
        System.out.println("Finish writing test words counter \n");
        
        
        numberOfWords = allWordsFormAllTraining.size();
        
        System.out.println("Start prediction");
        
        for (String tempFileName : allTestWords.keySet()){
            
            allTestFilesProb.put(tempFileName, new HashMap<String,Double>());
            if (!conf.containsKey(tempFileName.substring(0,tempFileName.indexOf("/"))))
                conf.put(tempFileName.substring(0,tempFileName.indexOf("/")), new HashMap<String,Integer>());
            
            
            
            for (String tempFileSubName : allTrainigWords.keySet())
                allTestFilesProb.get(tempFileName).put(tempFileSubName, Math.log10(prior.get(tempFileSubName)));
            
            for (String tempWord : allTestWords.get(tempFileName).keySet()){
                
                for (String className : allTrainigWords.keySet()){
                        
                    if (allTrainigWords.get(className).containsKey(tempWord)){
                        prevProb = allTestFilesProb.get(tempFileName).get(className);
                       
                        addToProb = Math.log10((allTrainigWords.get(className).get(tempWord) + 1D) 
                               / (wordConterForEveryClass.get(className) + numberOfWords));
                        
                        allTestFilesProb.get(tempFileName).put(className, prevProb + addToProb);
                        
                                                                             }//if statement
                    
                    else {
                        
                        prevProb = allTestFilesProb.get(tempFileName).get(className);
                        addToProb = Math.log10(1D / (wordConterForEveryClass.get(className) + numberOfWords + 1));
                        
                        allTestFilesProb.get(tempFileName).put(className, prevProb + addToProb);
                        
                         }//else statement
                                                                     }//for loop
                    
                
                                                                              }//for loop
            prob = -Global.Infinity;
            for (String tempProb : allTestFilesProb.get(tempFileName).keySet()){
                if (allTestFilesProb.get(tempFileName).get(tempProb) > prob){
                    prob = allTestFilesProb.get(tempFileName).get(tempProb);
                    classNameForTest = tempProb;
                                                                            }//if statement
                    
                                                                               }//for loop
            testPredection.put(tempFileName, classNameForTest);
            
            if (!conf.get(tempFileName.substring(0,tempFileName.indexOf("/"))).containsKey(classNameForTest))
                conf.get(tempFileName.substring(0,tempFileName.indexOf("/"))).put(classNameForTest, 1);
            
            else
                conf.get(tempFileName.substring(0,tempFileName.indexOf("/"))).put(classNameForTest
                        , conf.get(tempFileName.substring(0,tempFileName.indexOf("/")))
                                .get(classNameForTest) + 1);
                            
                    
            
                                                    }//for loop
        
        
        System.out.println("Finish predection \n");
        
        
            
            
            for (Entry<String, HashMap<String, Integer>> entry : conf.entrySet()) {
                
                
                truePositive = 0D;
                falsePositive = 0D;
                falseNegative = 0D;
                for (Entry<String, Integer> cEntry : entry.getValue().entrySet())
                    if (cEntry.getKey().equals(entry.getKey()))
                        truePositive = new Double(cEntry.getValue());
                    else
                        falsePositive += cEntry.getValue();
                                                                                 
                for (Entry<String, HashMap<String, Integer>> rEntry : conf.entrySet())
                    if (rEntry.getValue().containsKey(entry.getKey())) 
                        if (!rEntry.getKey().equals(entry.getKey()))
                            falseNegative += rEntry.getValue().get(entry.getKey());
                    
               
                recall = 0D;
                precision = 0D;
                F_Score = 0D;
                if (!(truePositive == 0 && falsePositive == 0))
                    recall = truePositive /  (truePositive + falsePositive);
                if (!(truePositive == 0 && falseNegative == 0))
                    precision = truePositive / (truePositive + falseNegative);
                if (!(recall == 0 && precision == 0))
                    F_Score = 2 * recall * precision / (recall + precision);
                totalF_Score += F_Score;
                System.out.println(entry.getKey() + " -fscore : " + F_Score);
            }
        
            System.out.println("Average F_Score : " + totalF_Score / prior.size());
            
            
        String tempClass;
        int i = 0;
        //System.out.println("Test Predection : ");
        for (String temp : testPredection.keySet()){
            tempClass = temp.substring(0,temp.indexOf("/"));
            //System.out.println(temp + " : " + testPredection.get(temp));
            if (tempClass.equals(testPredection.get(temp)))
                i++;
                                                   }//for loop*/
        
        
        System.out.println("Test prediction accuracy : " + (i + 0.0) / testPredection.size());
            
                                                               }//main
    
    
    public static void readStopWords () throws FileNotFoundException, IOException{
        
        BR = new BufferedReader(new InputStreamReader(new FileInputStream("Stop Words.txt")));
        
        while ((tempLine = BR.readLine()) != null)
            stopWords.put(tempLine, 0);
        
        BR.close();
                                                                                  }//readStopWords
    
    
    public static void writeTrainingWordsCountersToFiles () throws IOException{
        
        File temp = new File("training");

        if (!temp.exists())
            temp.mkdir();
        
        for (String tempFile : allTrainigWords.keySet()){
            
            BW = new BufferedWriter(new FileWriter(temp + "/" + tempFile + ".txt"));
            
            BW.write("" + prior.get(tempFile));
            BW.write(System.lineSeparator());
            
            for (String tempWord : allTrainigWords.get(tempFile).keySet()){
               
                BW.write(tempWord + " : " + allTrainigWords.get(tempFile).get(tempWord));
                BW.write(System.lineSeparator());
                                                                              }//for loop
            
            BW.close();
            
                                                            }//for loop

                                                                      }//writeWordsCountersToFiles
    
    
    public static void writeTestWordsCountersToFiles () throws IOException{
        
        File temp = new File("test");
        File dir = null;
        if (!temp.exists())
            temp.mkdir();
        
        for (String tempFile : allTestWords.keySet()){
            dir = new File (temp + "/" + tempFile.substring(0, tempFile.indexOf("/")));
            dir.mkdir();
                                                     }//for loop
        for (String tempFile : allTestWords.keySet()){
            
            BW = new BufferedWriter(new FileWriter(temp + "/" + tempFile.substring(0, tempFile.indexOf("/")) 
                    + tempFile.substring(tempFile.indexOf("/"),tempFile.length()) + ".txt"));
            
            for (String tempWord : allTestWords.get(tempFile).keySet()){
               
                BW.write(tempWord + " : " + allTestWords.get(tempFile).get(tempWord));
                BW.write(System.lineSeparator());
                                                                       }//for loop
            
            BW.close();
            
                                                            }//for loop

                                                                      }//writeWordsCountersToFiles
    
    
    public static void filesInDirTraining (File directory) throws IOException{
        File [] Temp = directory.listFiles();
        for (File Temp1 : Temp) 
            if (Temp1.isFile()) {
                
                priorCounter++;
                
                FileName = Temp1.getParentFile().getName();
                
                if (!allTrainigWords.containsKey(FileName))
                    allTrainigWords.put(FileName, new HashMap<String,Integer>());
                
                
                if (!prior.containsKey(FileName))
                    prior.put(FileName, 1D);
                
                else
                    prior.put(FileName, prior.get(FileName) + 1);
                
                BR = new BufferedReader(new InputStreamReader(new FileInputStream(Temp1)));
                
                while ((tempLine = BR.readLine())!= null){
                    
                        wordsInLine = tempLine.split("[\\s]");
                        tempLine = new String();
                        
                        for (int i = 0 ; i < wordsInLine.length ; i++)
                            if (!stopWords.containsKey(wordsInLine[i]))
                                tempLine += wordsInLine[i] + " ";
                    
                        lineAfterStemming = stemWords(tempLine);
                        
                        for (int i = 0 ; i < lineAfterStemming.size() ; i++){
                            
                            if (!allTrainigWords.get(FileName).containsKey(lineAfterStemming.get(i)))
                                allTrainigWords.get(FileName).put(lineAfterStemming.get(i), 1);
                        
                            else
                                allTrainigWords.get(FileName).put(lineAfterStemming.get(i), allTrainigWords
                                        .get(FileName).get(lineAfterStemming.get(i)) + 1);
                            
                            if (!allWordsFormAllTraining.containsKey(lineAfterStemming.get(i)))
                                allWordsFormAllTraining.put(lineAfterStemming.get(i), 1);
                        
                            else
                                allWordsFormAllTraining.put(lineAfterStemming.get(i), 
                                        allWordsFormAllTraining.get(lineAfterStemming.get(i)) + 1);
                            
                            if (!wordConterForEveryClass.containsKey(FileName))
                                wordConterForEveryClass.put(FileName, 1L);
                
                            else
                                wordConterForEveryClass.put(FileName, wordConterForEveryClass.get(FileName) + 1);
                                                                             }//for loop
                        
                                                          }//while loop
                
                                  }//if statement
                                
             else 
                filesInDirTraining(Temp1);
                                                                      }//filesInDirTrainig
    
    
    public static void filesInDirTest (File directory) throws IOException{
        File [] Temp = directory.listFiles();
        for (File Temp1 : Temp) 
            if (Temp1.isFile()) {
                
                FileName = Temp1.getParentFile().getName() + "/" + Temp1.getName();
                
                allTestWords.put(FileName, new HashMap<String,Integer> ());
               
                BR = new BufferedReader(new InputStreamReader(new FileInputStream(Temp1)));
                
                while ((tempLine = BR.readLine())!= null){
                    
                        wordsInLine = tempLine.split("[\\s]");
                        tempLine = new String();
                        
                        for (int i = 0 ; i < wordsInLine.length ; i++)
                            if (!stopWords.containsKey(wordsInLine[i]))
                                tempLine += wordsInLine[i] + " ";
                    
                        lineAfterStemming = stemWords(tempLine);
                        
                        for (int i = 0 ; i < lineAfterStemming.size() ; i++){
                            
                            if (!allTestWords.get(FileName).containsKey(lineAfterStemming.get(i)))
                                allTestWords.get(FileName).put(lineAfterStemming.get(i), 1);
                            
                            else 
                                allTestWords.get(FileName).put(lineAfterStemming.get(i), 
                                        allTestWords.get(FileName).get(lineAfterStemming.get(i)) + 1);
                                                     
                                                                             }//for loop
                        
                                                          }//while loop
                
                                  }//if statement
                                
             else 
                filesInDirTest(Temp1);
                                                                      }//filesInDir
    
    
    public static Vector stemWords (String statemet){
        
        Vector <String> stemmedVector = new Vector<>();
        
        Integer count;
        
        int counter , sT;
        statemet += " ";
        String temp = "";
        
        PorterStemmer stemmer = new PorterStemmer();
        
        for (int i = 0 ; i < statemet.length() ;){
            
            sT = statemet.charAt(i);
            
            if (!Character.isLetter(sT)){
                i++;
                continue;
                                        }//if statement
            
            if (Character.isLetter((char)sT)){
                
                counter = 0;
                
                while (i < statemet.length()){
                    
                    sT = Character.toLowerCase((char)sT);
                    temp += (char)sT;
                    if (counter < 500)
                        counter++;
                    i++;
                    sT = statemet.charAt(i);

                    if (!Character.isLetter((char) sT)) {
                                                         
                        for (int c = 0; c < counter; c++)
                            stemmer.add(temp.charAt(c));
                        
                        stemmer.stem();
                        
                        if (!temp.equals("")){
                            
                            temp = stemmer.toString();
                            
                            stemmedVector.addElement(temp);
                            
                                             }//if statement
                        
                        temp = "";
                        i++;
                        break;
                                                        }//if statement
                    
                             }//while loop
                
                                               }//if statement
            
                    if (sT < 0)
                        break;
                 
                              
                    
                       }//while loop
     
        
        
        return stemmedVector;
        
                                                        }//stemToPredect
    
                                 }//TextClassification   